OCB_FLAGS = -use-ocamlfind -use-menhir -I src
OCB = 		ocamlbuild $(OCB_FLAGS)

LIB_FILES = log.cma log.cmxa log.a log.cmi log.cmx log.cmxs
INSTALL_FILES = $(LIB_FILES:%=_build/src/%)

VERSION = `cat VERSION`

build:
	$(OCB) $(LIB_FILES)

install: build uninstall
	ocamlfind install -patch-version $(VERSION) log META $(INSTALL_FILES)

uninstall:
	ocamlfind remove log

doc:
	$(OCB) log.docdir/index.html

.PHONY:	all clean build

clean:
	$(OCB) -clean
