open ANSITerminal

(** Log module *)
module Log : sig

  (** Level of messages

      		Optional :
      		{ul
      			{- [`DEBUG] : display debug messages}
      			{- [`INFO] : display information messages}
      			{- [`MESSAGE] : display neutral messages}
      			{- [`WARNING] : display warning messages}
      		}

      		By default (can't be changed):
      		{ul
      			{- [`CRITICAL] : display critical messages and quit the main program}
      			{- [`BUG] : display bug messages}
      		}

      	*)
  type level = [ `DEBUG | `INFO | `MESSAGE | `WARNING]


  (** Style [bold] *)
  val bold: ANSITerminal.style

  (** Style [inverse] *)
  val inverse: ANSITerminal.style

  (** Style [underlined] *)
  val underlined: ANSITerminal.style


  (** {2 Foreground colors} *)

  (** Blue *)
  val f_blue: ANSITerminal.style

  (** Default (terminal foreground's color)*)
  val f_default: ANSITerminal.style

  (** Black *)
  val f_black: ANSITerminal.style

  (** Red *)
  val f_red: ANSITerminal.style

  (** Green *)
  val f_green: ANSITerminal.style

  (** Yellow *)
  val f_yellow: ANSITerminal.style

  (** Magenta *)
  val f_magenta: ANSITerminal.style

  (** Cyan *)
  val f_cyan: ANSITerminal.style

  (** White *)
  val f_white: ANSITerminal.style

  (** {2 Background colors} *)

  (** Blue *)
  val b_blue: ANSITerminal.style

  (** Default (terminal's background color) *)
  val b_default: ANSITerminal.style

  (** Black *)
  val b_black: ANSITerminal.style

  (** Red *)
  val b_red: ANSITerminal.style

  (** Green *)
  val b_green: ANSITerminal.style

  (** Yellow *)
  val b_yellow: ANSITerminal.style

  (** Magenta *)
  val b_magenta: ANSITerminal.style

  (** Cyan *)
  val b_cyan: ANSITerminal.style

  (** White *)
  val b_white: ANSITerminal.style

  (** {2 Log's configuration} *)

  (** {3 Label} *)

  (** Set the warning label *)
  val set_warning_label: string -> unit

  (** Set the bug label *)
  val set_bug_label: string -> unit

  (** Set the debug label *)
  val set_debug_label: string -> unit

  (** Set the info label *)
  val set_info_label: string -> unit

  (** Set the critical label *)
  val set_critical_label: string -> unit

  (** Set the bug label *)
  val set_bug_label: string -> unit

  (** {3 Style} *)

  (** Set the warning foreground color *)
  val set_warning_foreground: ANSITerminal.style -> unit

  (** Set the warning background color *)
  val set_warning_background: ANSITerminal.style -> unit

  (** Set the warning styles color *)
  val set_warning_style: ANSITerminal.style list -> unit

  (** Set the message foreground color *)
  val set_message_foreground: ANSITerminal.style -> unit

  (** Set the message background color *)
  val set_message_background: ANSITerminal.style -> unit

  (** Set the message styles color *)
  val set_message_style: ANSITerminal.style list -> unit

  (** Set the info foreground color *)
  val set_info_foreground: ANSITerminal.style -> unit

  (** Set the info background color *)
  val set_info_background: ANSITerminal.style -> unit

  (** Set the info styles color *)
  val set_info_style: ANSITerminal.style list -> unit

  (** Set the bug foreground color *)
  val set_bug_foreground: ANSITerminal.style -> unit

  (** Set the bug background color *)
  val set_bug_background: ANSITerminal.style -> unit

  (** Set the bug styles color *)
  val set_bug_style: ANSITerminal.style list -> unit

  (** Set the debug foreground color *)
  val set_debug_foreground: ANSITerminal.style -> unit

  (** Set the debug background color *)
  val set_debug_background: ANSITerminal.style -> unit

  (** Set the debug styles color *)
  val set_debug_style: ANSITerminal.style list -> unit

  (** Set the critical foreground color *)
  val set_critical_foreground: ANSITerminal.style -> unit

  (** Set the critical background color *)
  val set_critical_background: ANSITerminal.style -> unit

  (** Set the critical styles color *)
  val set_critical_style: ANSITerminal.style list -> unit

  (** Display or Hide time in logs *)
  val set_show_time: bool -> unit

  (** Set the log file to write logs *)
  val set_log_file: string -> unit

  (** Write or not logs in the logs file *)
  val set_write_to_log_file: bool -> unit

  (** Set active levels
      		@param levels a list of levels
      	*)
  val set_active_levels: levels:(level list) -> unit



  (** {2 Adding custom level} *)

  (** Add a custom level *)
  val add_custom_level : level:int -> background:ANSITerminal.style -> foreground:ANSITerminal.style -> label:string -> exit_at_end:bool -> unit

  (** Call a custom level *)
  val custom_level : level:int -> msg:string -> unit

  (** {2 Displaying messages calling} *)

  (** Display (and write into log file) critical messages (from string/in Printf style) *)
  val critical: string -> 'a
  val fcritical: ('a, unit, string, 'b) format4 -> 'a

  (** Display (and write into log file) neutral messages *)
  val message: string -> unit
  val rmessage: string -> unit
  val fmessage: ('a, unit, string, unit) format4 -> 'a
  val frmessage: ('a, unit, string, unit) format4 -> 'a

  (** Display (and write into log file) bug messages *)
  val bug: string -> unit
  val rbug: string -> unit
  val fbug: ('a, unit, string, unit) format4 -> 'a
  val frbug: ('a, unit, string, unit) format4 -> 'a

  (** Display (and write into log file) debug messages *)
  val debug: string -> unit
  val rdebug: string -> unit
  val fdebug: ('a, unit, string, unit) format4 -> 'a
  val frdebug: ('a, unit, string, unit) format4 -> 'a

  (** Display (and write into log file) warning messages *)
  val warning: string -> unit
  val rwarning: string -> unit
  val fwarning: ('a, unit, string, unit) format4 -> 'a
  val frwarning: ('a, unit, string, unit) format4 -> 'a

  (** Display (and write into log file) info messages *)
  val info: string -> unit
  val rinfo: string -> unit
  val finfo: ('a, unit, string, unit) format4 -> 'a
  val frinfo: ('a, unit, string, unit) format4 -> 'a
end
