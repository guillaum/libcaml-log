module T = ANSITerminal

let r = ref false


module IntMap = Map.Make(struct
    type t = int
    let compare = compare
  end)

module Log = struct


  type level = [
      `WARNING
    | `DEBUG
    | `INFO
    | `MESSAGE
  ]

  let f_default = T.default
  let f_blue = T.blue
  let f_black = T.black
  let f_red = T.red
  let f_green = T.green
  let f_yellow = T.yellow
  let f_magenta = T.magenta
  let f_cyan = T.cyan
  let f_white = T.white

  let b_default = T.on_default
  let b_blue = T.on_blue
  let b_black = T.on_black
  let b_red = T.on_red
  let b_green = T.on_green
  let b_yellow = T.on_yellow
  let b_magenta = T.on_magenta
  let b_cyan = T.on_cyan
  let b_white = T.on_white


  let bold = T.Bold
  let underlined = T.Underlined
  let inverse = T.Inverse

  let show_time = ref false

  let warning_label = ref "WARNING"
  let debug_label = ref "DEBUG"
  let critical_label = ref "CRITICAL"
  let bug_label = ref "<<BUG>>"
  let message_label = ref "MESSAGE"
  let info_label = ref "INFO"

  let log_file = ref "out.log"
  let write_to_log_file = ref false

  let warning_foreground = ref f_blue
  let warning_background = ref b_default
  let warning_style = ref []
  let set_warning_foreground foreground = warning_foreground := foreground
  let set_warning_background background = warning_background := background
  let set_warning_style style = warning_style := style

  let debug_foreground = ref f_green
  let debug_background = ref b_default
  let debug_style = ref []
  let set_debug_foreground foreground = debug_foreground := foreground
  let set_debug_background background = debug_background := background
  let set_debug_style style = debug_style := style

  let info_foreground = ref f_white
  let info_background = ref b_default
  let info_style = ref []
  let set_info_foreground foreground = info_foreground := foreground
  let set_info_background background = info_background := background
  let set_info_style style = info_style := style

  let message_foreground = ref f_yellow
  let message_background = ref b_default
  let message_style = ref []
  let set_message_foreground foreground = message_foreground := foreground
  let set_message_background background = message_background := background
  let set_message_style style = message_style := style

  let bug_foreground = ref f_red
  let bug_background = ref b_default
  let bug_style = ref [bold]
  let set_bug_foreground foreground = bug_foreground := foreground
  let set_bug_background background = bug_background := background
  let set_bug_style style = bug_style := style

  let critical_foreground = ref f_white
  let critical_background = ref b_red
  let critical_style = ref [bold]
  let set_critical_foreground foreground = critical_foreground := foreground
  let set_critical_background background = critical_background := background
  let set_critical_style style = critical_style := style


  let active_levels = ref []


  let set_active_levels ~levels =
    active_levels := levels

  let set_show_time show = show_time := show

  let set_log_file file = log_file := file
  let set_write_to_log_file b = write_to_log_file := b


  let set_warning_label label = warning_label := label
  let set_bug_label label = bug_label := label
  let set_debug_label label = debug_label := label
  let set_info_label label = info_label := label
  let set_critical_label label = critical_label := label
  let set_bug_label label = bug_label := label


  let get_time () =
    if (!show_time) then (
      let time = Unix.localtime (Unix.time ()) in
      Printf.sprintf "[%02d/%02d/%04d %02d:%02d:%02d] " time.Unix.tm_mday (time.Unix.tm_mon+1) (time.Unix.tm_year+1900) time.Unix.tm_hour time.Unix.tm_min time.Unix.tm_sec
    ) else (
      ""
    )

  let write_log_file to_write =
    if (!write_to_log_file) then (
      try
        let out_ch = open_out_gen [Open_append] 0o644 !log_file in
        output_string out_ch to_write;
        flush out_ch;
        close_out out_ch
      with Sys_error msg ->
        T.eprintf ([!warning_foreground; !warning_background]@(!warning_style)) "%s%8s : %s\n%!" (get_time ()) !warning_label msg;
        flush stderr;
    )




  let warning msg =
    if (List.mem `WARNING !active_levels) then (
      if (!r) then ( Printf.printf "\n");
      r := false;
      T.eprintf ([!warning_foreground; !warning_background]@(!warning_style)) "%s%8s : %s\n%!" (get_time ()) !warning_label msg;
      flush stderr;
      write_log_file (Printf.sprintf "%s%8s : %s\n%!" (get_time ()) !warning_label msg);
    )

  let fwarning msg = Printf.ksprintf warning msg

  let rwarning msg =
    r := true;
    if (List.mem `WARNING !active_levels) then (
      T.eprintf ([!warning_foreground; !warning_background]@(!warning_style)) "\r%s%8s : %s%!" (get_time ()) !warning_label msg;
      flush stderr;
      write_log_file (Printf.sprintf "%s%8s : %s\n%!" (get_time ()) !warning_label msg);
    )

  let frwarning msg = Printf.ksprintf rwarning msg

  let debug msg =
    if (List.mem `DEBUG !active_levels) then (
      if (!r) then ( Printf.printf "\n");
      r := false;
      T.eprintf ([!debug_foreground; !debug_background]@(!debug_style))  "%s%8s : %s\n%!" (get_time ()) !debug_label msg;
      flush stderr;
      write_log_file (Printf.sprintf "%s%8s : %s\n%!" (get_time ()) !debug_label msg);
    )

  let fdebug msg = Printf.ksprintf debug msg

  let rdebug msg =
    r := true;
    if (List.mem `DEBUG !active_levels) then (
      T.eprintf ([!debug_foreground; !debug_background]@(!debug_style))  "\r%s%8s : %s%!" (get_time ()) !debug_label msg;
      flush stderr;
      write_log_file (Printf.sprintf "%s%8s : %s\n%!" (get_time ()) !debug_label msg);
    )

  let frdebug msg = Printf.ksprintf rdebug msg

  let info msg =
    if (List.mem `INFO !active_levels) then (
      if (!r) then ( Printf.printf "\n");
      r := false;
      T.eprintf ([!info_foreground; !info_background]@(!info_style)) "%s%8s : %s\n%!" (get_time ()) !info_label msg;
      flush stderr;
      write_log_file (Printf.sprintf "%s%8s : %s\n%!" (get_time ()) !info_label msg);
    )

  let finfo msg = Printf.ksprintf info msg

  let rinfo msg =
    r := true;
    if (List.mem `INFO !active_levels) then (
      T.eprintf ([!info_foreground; !info_background]@(!info_style)) "\r%s%8s : %s%!" (get_time ()) !info_label msg;
      flush stderr;
      write_log_file (Printf.sprintf "%s%8s : %s\n%!" (get_time ()) !info_label msg);
    )

  let frinfo msg = Printf.ksprintf rinfo msg

  let message msg =
    if (List.mem `MESSAGE !active_levels) then (
      if (!r) then ( Printf.printf "\n");
      r := false;
      T.eprintf ([!message_foreground; !message_background]@(!message_style)) "%s%8s : %s\n%!" (get_time ()) !message_label msg;
      flush stderr;
      write_log_file (Printf.sprintf "%s%8s : %s\n%!" (get_time ()) !message_label msg);
    )

  let fmessage msg = Printf.ksprintf message msg

  let rmessage msg =
    r := true;
    if (List.mem `MESSAGE !active_levels) then (
      T.eprintf ([!message_foreground; !message_background]@(!message_style)) "\r%s%8s : %s%!" (get_time ()) !message_label msg;
      flush stderr;
      write_log_file (Printf.sprintf "%s%8s : %s\n%!" (get_time ()) !message_label msg);
    )

  let frmessage msg = Printf.ksprintf rmessage msg


  let bug msg =
    if (!r) then ( Printf.printf "\n");
    r := false;
    T.eprintf ([!bug_foreground; !bug_background]@(!bug_style)) "%s%8s : %s\n%!" (get_time ()) !bug_label msg;
    flush stderr;
    write_log_file (Printf.sprintf "%s%8s : %s\n%!" (get_time ()) !bug_label msg)

  let fbug msg = Printf.ksprintf bug msg

  let rbug msg =
    r := true;
    T.eprintf ([!bug_foreground; !bug_background]@(!bug_style)) "\r%s%8s : %s%!" (get_time ()) !bug_label msg;
    flush stderr;
    write_log_file (Printf.sprintf "%s%8s : %s\n%!" (get_time ()) !bug_label msg)

  let frbug msg = Printf.ksprintf rbug msg

  let critical msg =
    if (!r) then ( Printf.printf "\n");
    r := false;
    T.eprintf ([!critical_foreground; !critical_background]@(!critical_style)) "%s%8s : %s\n%!" (get_time ()) !critical_label msg;
    flush stderr;
    write_log_file (Printf.sprintf "%s%8s : %s\n%!" (get_time ()) !critical_label msg);
    exit 1

  let fcritical msg = Printf.ksprintf critical msg



  let custom_level = ref IntMap.empty

  let add_custom_level ~level ~background ~foreground ~label ~exit_at_end =
    try
      ignore(IntMap.find level !custom_level);
      (* fail *)
      T.eprintf ([!warning_foreground; !warning_background]@(!warning_style)) "%s%8s : %s\n%!" (get_time ()) "WARNING" (Printf.sprintf "Level '%d' already registered!" level);
      flush stderr;
      write_log_file (Printf.sprintf "%s%8s : %s\n%!" (get_time ()) "WARNING" (Printf.sprintf "Level '%d' already registered!" level));
    with Not_found ->
      (* ok *)
      custom_level := IntMap.add level (label,background,foreground,exit_at_end) !custom_level


  let custom_level ~level ~msg =
    try
      let (label,background,foreground,exit_at_end) = IntMap.find level !custom_level in
      (* ok *)
      T.eprintf [foreground; background] "%s%8s : %s\n%!" (get_time ()) label msg;
      flush stderr;
      write_log_file (Printf.sprintf "%s%8s : %s\n%!" (get_time ()) label msg);
      if (exit_at_end) then (exit(0));
    with Not_found ->
      (* fail *)
      T.eprintf ([!warning_foreground; !warning_background]@(!warning_style)) "%s%8s : %s\n%!" (get_time ()) "WARNING" (Printf.sprintf "Level '%d' unregistered!" level);
      flush stderr;
      write_log_file (Printf.sprintf "%s%8s : %s\n%!" (get_time ()) "WARNING" (Printf.sprintf "Level '%d' unregistered!" level));

end

let exit_line () = if !r then Printf.printf "\n%!"
let _ = at_exit exit_line
